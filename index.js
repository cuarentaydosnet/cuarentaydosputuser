const requestJson = require('request-json');
const argon2 = require('argon2');

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;

    let body = JSON.parse(event.body);
    var httpClient = requestJson.createClient(db);
    
    //Si es usuario normal, solo puede editar sus datos
    if(user.tipo == 0){
        if(body._id == event.pathParameters.accountid){
            var newUser = {
                "_id" : event.pathParameters.accountid,
                "nombre" : body.nombre,
                "apellido1" : body.apellido1,
                "apellido2" : body.apellido2,
                "email" : body.email,
                "fecha_alta" : user.fecha_alta,
                "tipo" : user.tipo,
                "activo" : body.activo,
                "password" : user.password
            };
            console.log(newUser);        
            httpClient.post("user?" + apikey, newUser, function(err,resMLab,body){
                if(err){
                    var response = {
                        "statusCode" : 500,
                        "body": JSON.stringify(err)
                    };
                } else {
                    response = { 
                        "statusCode": 200,
                        "headers" : {
                            "Access-Control-Allow-Origin": "*"
                        },                        
                        "body": JSON.stringify(body),
                    };
                }
                return callback(null,response);
            });
        } else { // un usuario normal está intentando modifcar datos de otro usuario. No se puede
            var response = { 
                "statusCode": 401,
                "body": '{"msg" : "Solo puedes modificar tus datos"}',
            };
        return callback(null,response)
        }
    } else { // Es usuario con privilegios, puede modificar los datos de cualqueir usuario
        
    
    
    
        argon2.hash(body.password, {type: argon2.argon2id}).then(hash => {
            var newUser = {
                "_id" : event.pathParameters.accountid,
                "nombre" : body.nombre,
                "apellido1" : body.apellido1,
                "apellido2" : body.apellido2,
                "email" : body.email,
                "fecha_alta" : body.fecha_alta,
                "tipo" : body.tipo,
                "activo" : body.activo,
                "password" : hash
            };
            console.log(newUser);
            //persistir datos
            var httpClient = requestJson.createClient(db);
            httpClient.post("user?" + apikey, newUser, function(err,resMLab,body){
                if(err){
                    response = {
                        "statusCode" : 500,
                        "body": JSON.stringify(err),
                        "isBase64Encoded": false
                    };
                } else {
                    response = { 
                        "statusCode": 200,
                        "headers" : {
                            "Access-Control-Allow-Origin": "*"
                        },                        
                        "body": JSON.stringify(body),
                        "isBase64Encoded": false
                    };
                }
                return callback(null,response);
            });
        })
    }
};